package rs.ac.singidunum.memento;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private Context context;
    private Activity activity;
    private ArrayList task_id, task_name, task_description, task_day, task_time_from, task_time_to, task_all_day, task_completed;

    CustomAdapter(Activity activity, Context context, ArrayList task_id, ArrayList task_name, ArrayList task_description,
                  ArrayList task_day, ArrayList task_time_from, ArrayList task_time_to, ArrayList task_all_day, ArrayList task_completed){
        this.activity = activity;
        this.context = context;
        this.task_id = task_id;
        this.task_name = task_name;
        this.task_description = task_description;
        this.task_day = task_day;
        this.task_time_from = task_time_from;
        this.task_time_to = task_time_to;
        this.task_all_day = task_all_day;
        this.task_completed = task_completed;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.task_id_txt.setText(String.valueOf(task_id.get(position)));
        holder.task_name_txt.setText(String.valueOf(task_name.get(position)));
        holder.task_description_txt.setText(String.valueOf(task_description.get(position)));
        holder.task_day_txt.setText(String.valueOf(task_day.get(position)));
        if(String.valueOf(task_completed.get(position)).equals("1")){
            holder.check_imageview.setVisibility(View.VISIBLE);
        }
        //Recyclerview onClickListener
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, rs.ac.singidunum.memento.UpdateActivity.class);
                intent.putExtra("id", String.valueOf(task_id.get(position)));
                intent.putExtra("name", String.valueOf(task_name.get(position)));
                intent.putExtra("description", String.valueOf(task_description.get(position)));
                intent.putExtra("day", String.valueOf(task_day.get(position)));
                intent.putExtra("time_from", String.valueOf(task_time_from.get(position)));
                intent.putExtra("time_to", String.valueOf(task_time_to.get(position)));
                intent.putExtra("all_day", String.valueOf(task_all_day.get(position)));
                intent.putExtra("completed", String.valueOf(task_completed.get(position)));
                activity.startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return task_id.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView task_id_txt, task_name_txt, task_description_txt, task_day_txt;
        ImageView check_imageview;
        LinearLayout mainLayout;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            task_id_txt = itemView.findViewById(R.id.task_id_txt);
            task_name_txt = itemView.findViewById(R.id.task_name_txt);
            task_description_txt = itemView.findViewById(R.id.task_description_txt);
            task_day_txt = itemView.findViewById(R.id.task_day_txt);
            check_imageview=itemView.findViewById(R.id.check_imageview);;

            mainLayout = itemView.findViewById(R.id.mainLayout);
            //Animate Recyclerview
            Animation translate_anim = AnimationUtils.loadAnimation(context, R.anim.translate_anim);
            mainLayout.setAnimation(translate_anim);
        }

    }

}
