package rs.ac.singidunum.memento;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddTaskActivity extends AppCompatActivity {

    EditText name_input, description_input;
    TextView time_from_label, time_to_label, day_label;
    Switch all_day_switch;
    Button add_button, time_from_button, time_to_button, date_picker_button;
    boolean allDayValue;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        name_input = findViewById(R.id.name_input);
        description_input = findViewById(R.id.description_input);
        day_label = findViewById(R.id.day_input);
        time_from_button = findViewById(R.id.time_from);
        time_to_button = findViewById(R.id.time_to);
        time_from_label = findViewById(R.id.time_from_label);
        time_to_label = findViewById(R.id.time_to_label);
        all_day_switch = findViewById(R.id.all_Day);
        add_button = findViewById(R.id.add_button);
        date_picker_button = findViewById(R.id.datePicker);


        saveTask(); //add_button onClickListener
        setDay(); // datePicker
        setTimeFrom(); //time_from_button onClickListener
        setTimeTo(); //time_to_button onClickListener
        setAllDay(); //all_day_switch setOnCheckedChangeListener

    }

    private void setAllDay() {
            //attach a listener to check for changes in state
            all_day_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {


                //If allDay is checked, we disable time from and to and vice versa
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {

                    if(isChecked){
                        allDayValue = true;
                        time_to_button.setEnabled(false);
                        time_from_button.setEnabled(false);
                        time_to_label.setText("");
                        time_from_label.setText("");
                        time_to_label.setEnabled(false);
                        time_from_label.setEnabled(false);
                    }else{
                        allDayValue = false;
                        time_to_button.setEnabled(true);
                        time_from_button.setEnabled(true);
                        time_from_label.setEnabled(true);
                        time_to_label.setEnabled(true);
                    }
                }
            });

        }

    private void setTimeTo() {
        time_to_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Process to get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // Display Selected time in textbox
                                time_to_label.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, true);
                tpd.show();
            }
        });

    }

    private void setTimeFrom() {
        time_from_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Process to get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);


                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // Display Selected time in textbox
                                time_from_label.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, true);
                tpd.show();
            }
        });
    }

    private void setDay() {
        date_picker_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // Display Selected date in textbox
                                day_label = (TextView)findViewById(R.id.day_input);
                                Date temp=new Date(year-1900,monthOfYear, dayOfMonth);
                                SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
                                String dateText = df2.format(temp);
                                day_label.setText(dateText);


                            }
                        }, mYear, mMonth, mDay);
                //Set min date to today
                dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dpd.show();
            }
        });
    }

    private void saveTask() {
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = name_input.getText().toString().trim();
                String day = day_label.getText().toString().trim();
                if(TextUtils.isEmpty(name)) {
                    Toast.makeText(context, "Enter task name ", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(day)){
                    Toast.makeText(context, "Enter task day ", Toast.LENGTH_SHORT).show();
                    return;
                }

                MyDatabaseHelper myDB = new MyDatabaseHelper(AddTaskActivity.this);
                myDB.addTask(name_input.getText().toString().trim(),
                        description_input.getText().toString().trim(),
                        day_label.getText().toString().trim(),
                        allDayValue,
                        time_from_label.getText().toString().trim(),
                        time_to_label.getText().toString().trim());
                //Refresh Activity
                Intent intent = new Intent(AddTaskActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
