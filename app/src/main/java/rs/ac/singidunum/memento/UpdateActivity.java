package rs.ac.singidunum.memento;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UpdateActivity extends AppCompatActivity {

    EditText name_input, description_input;
    TextView time_from_label, time_to_label, day_label;
    Button update_button, delete_button, date_picker_button2, time_from_button2,time_to_button2;
    Switch completed_switch, all_day_switch;
    String id, name, description, day, timeTo, timeFrom;
    boolean allDay, completed;


    private int mYear, mMonth, mDay, mHour, mMinute;
    private Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        name_input = findViewById(R.id.name_input2);
        description_input = findViewById(R.id.description_input2);

        completed_switch = findViewById(R.id.completed);
        all_day_switch = findViewById(R.id.all_Day2);
        day_label = findViewById(R.id.day_input_2);
        date_picker_button2 = findViewById(R.id.datePicker2);
        time_from_button2 = findViewById(R.id.time_from2);
        time_from_label= findViewById(R.id.time_from_label2);
        time_to_button2 = findViewById(R.id.time_to2);
        time_to_label= findViewById(R.id.time_to_label2);
        update_button = findViewById(R.id.update_button);
        delete_button = findViewById(R.id.delete_button);

        //First we call this
        getAndSetIntentData();

        setAllDay(); //all_day_switch setOnCheckedChangeListener
        setCompleted(); //completed_switch setOnCheckedChangeListener
        setDay(); // datePicker
        setTimeFrom(); //time_from_button onClickListener
        setTimeTo(); //time_to_button onClickListener

        //Set actionbar title after getAndSetIntentData method
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(name);
        }

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //And only then we call this
                MyDatabaseHelper myDB = new MyDatabaseHelper(UpdateActivity.this);
                name = name_input.getText().toString().trim();
                description = description_input.getText().toString().trim();
                day = day_label.getText().toString().trim();
                timeFrom =time_from_label.getText().toString().trim();
                timeTo = time_to_label.getText().toString().trim();
                if(TextUtils.isEmpty(name)) {
                    Toast.makeText(context, "Enter task name ", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(day)){
                    Toast.makeText(context, "Enter task day ", Toast.LENGTH_SHORT).show();
                    return;
                }
                myDB.updateData(id, name, description, day, allDay, timeTo, timeFrom, completed);
                //Refresh Activity
                Intent intent = new Intent(UpdateActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmDialog();
            }
        });

    }

    void getAndSetIntentData(){
        if(getIntent().hasExtra("id") && getIntent().hasExtra("name") &&
                getIntent().hasExtra("description") && getIntent().hasExtra("day")
                && getIntent().hasExtra("time_from") && getIntent().hasExtra("time_to") && getIntent().hasExtra("all_day") && getIntent().hasExtra("completed")){
            //Getting Data from Intent
            id = getIntent().getStringExtra("id");
            name = getIntent().getStringExtra("name");
            description = getIntent().getStringExtra("description");
            day = getIntent().getStringExtra("day");
            timeTo =  getIntent().getStringExtra("time_to");
            timeFrom=  getIntent().getStringExtra("time_from");
            String allDayString =  getIntent().getStringExtra("all_day");

            if (allDayString.equals("1")) {
                all_day_switch.setChecked(true);
            }
            String completedString =  getIntent().getStringExtra("completed");
            if (completedString.equals("1")) {
                //completed = true;
                completed_switch.setChecked(true);
            }

            //Setting Intent Data
            name_input.setText(name);
            description_input.setText(description);
            day_label.setText(day);
            time_to_label.setText(timeTo);
            time_from_label.setText(timeFrom);

        }else{
            Toast.makeText(this, "No tasks", Toast.LENGTH_SHORT).show();
        }
    }

    void confirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete " + name + " ?");
        builder.setMessage("Are you sure you want to delete " + name + " ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MyDatabaseHelper myDB = new MyDatabaseHelper(UpdateActivity.this);
                myDB.deleteOneRow(id);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }

    private void setAllDay() {
        //attach a listener to check for changes in state
        all_day_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {


            //If allDay is checked, we disable time from and to and vice versa
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    allDay = true;
                    time_to_button2.setEnabled(false);
                    time_from_button2.setEnabled(false);
                    time_to_label.setText("");
                    time_from_label.setText("");
                    time_to_label.setEnabled(false);
                    time_from_label.setEnabled(false);
                }else{
                    allDay = false;
                    time_to_button2.setEnabled(true);
                    time_from_button2.setEnabled(true);
                    time_from_label.setEnabled(true);
                    time_to_label.setEnabled(true);
                }
            }
        });
    }

    private void setCompleted() {
        //attach a listener to check for changes in state
        completed_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    completed = true;
                    Toast.makeText(getApplicationContext(), "Task completed", Toast.LENGTH_SHORT).show();

                }else{
                    completed = false;
                    Toast.makeText(getApplicationContext(), "Task active", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setTimeTo() {
        time_to_button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Process to get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // Display Selected time in textbox
                                time_to_label.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, true);
                tpd.show();
            }
        });

    }

    private void setTimeFrom() {
        time_from_button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Process to get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);


                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // Display Selected time in textbox
                                time_from_label.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, true);
                tpd.show();
            }
        });
    }

    private void setDay() {
        date_picker_button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // Display Selected date in textbox
                                day_label = (TextView)findViewById(R.id.day_input_2);
                                Date temp=new Date(year-1900,monthOfYear, dayOfMonth);
                                SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
                                String dateText = df2.format(temp);
                                day_label.setText(dateText);


                            }
                        }, mYear, mMonth, mDay);
                //set min date to today
                dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dpd.show();
            }
        });
    }
}
